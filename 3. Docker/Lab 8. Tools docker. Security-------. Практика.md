#### Tools docker, Security, Практика (4 часа)

#### Tools docker
##### HADOLINT

Удобный линтер Dockerfile, который помогает создавать правильные Dockerfile, согласно лучших практик.

Скачаем image
`docker pull hadolint/hadolint`

Перейдите в каталог с вашим Dockerfile
`docker run --rm -i hadolint/hadolint < Dockerfile`
Перед вами два вывода
```output
-:3 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:4 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:4 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
-:4 DL3019 info: Use the `--no-cache` switch to avoid the need to use `--update` and remove `/var/cache/apk/*` when done installing packages
-:8 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
-:11 DL3018 warning: Pin versions in apk add. Instead of `apk add <package>` use `apk add <package>=<version>`
-:15 DL3025 warning: Use arguments JSON notation for CMD and ENTRYPOINT arguments
```
```output
-:2 DL3027 warning: Do not use apt as it is meant to be a end-user tool, use apt-get or apt-cache instead
-:8 DL3042 warning: Avoid use of cache directory with pip. Use `pip install --no-cache-dir <package>`
-:8 DL3059 info: Multiple consecutive `RUN` instructions. Consider consolidation.
```

##### Dive
Инструмент для изучения содержимого слоев образа Docker и поиска способов уменьшить его размер. 
Установка
wget https://github.com/wagoodman/dive/releases/download/v0.9.2/dive_0.9.2_linux_amd64.deb
sudo apt install ./dive_0.9.2_linux_amd64.deb
Запустим
dive flask_mainapp:latest
```
Cmp   Size  Command                               ├── bin
     69 MB  FROM a0441b36624a46d                  │   ├── bash
    7.0 MB  set -eux;  apt-get update;  apt-get i │   ├── cat           
     29 MB  set -ex   && savedAptMark="$(apt-mark │   ├── chgrp   
       0 B  cd /usr/local/bin  && ln -s idle3 idl │   ├── chmod        
    9.5 MB  set -ex;   savedAptMark="$(apt-mark s │   ├── chown       
     21 MB  apt update -y && apt upgrade -y       │   ├── cp        
       0 B  #(nop) WORKDIR /app                   │   ├── dash           
     780 B  #(nop) COPY file:dee2049857c0d2462dfd │   ├── date     
     15 MB  /usr/local/bin/python -m pip install  │   ├── dd                           
    129 MB  pip3 install -r requirements.txt      │   ├── df                       
     63 MB  #(nop) COPY dir:20622e83d6174117e1202 │   ├── dir                              
                                                  │   ├── dmesg                     
│ Layer Details ├──────────────────────────────── │   ├── dnsdomainname → hostname     
                                                  │   ├── domainname → hostname    
Tags:   (unavailable)                             │   ├── echo                           
Id:     4cd949ff443ee86d59b852cd897a3706fee36c0f7 │   ├── egrep                  
dcf81afb6f867b33d506679                           │   ├── false                    
Digest: sha256:ddc260ab913e1beb894c1ab211bec2b938 │   ├── fgrep                     
9dbb2d3536b96d43a46cf059c13b8e                    │   ├── findmnt                 
```

#### Security
Недавняя атака на инфраструктуру Kubernetes Tesla заставила всех понять, что переход от Bare-Metal-Machines к Virtual-Machines, вплоть до контейнеров, никогда не устраняет лазейки в безопасности, которые часто остаются без внимания. Что ж, есть несколько лучших практик с точки зрения безопасности, которым можно следовать при Dockerizing и приложениях, таких как забота о секретах / учетных данных, избегайте использования пользователя root в качестве пользователя по умолчанию для контейнера и нескольких других. Лучшим подходом к борьбе с уязвимостями безопасности в сфере контейнеров является включение инструментов / технологий, ориентированных на выполнение надежных проверок безопасности по отношению к контейнеру, который присутствует в вашей среде. Есть несколько инструментов, которые можно добавить в ваш арсенал безопасности.

##### Dockle
Dockle проверяет корректность и безопасность конкретного образа как такового, анализируя его слои и конфигурацию – какие пользователи созданы, какие инструкции используются, какие тома подключены, присутствие пустого пароля и т.д. Таким образом функционал частично пересекается с DBS, но позволяет сканировать конкретный образ и за счёт простоты – можно легко внедрить этот инструмент в CI/CD.

Качаем последнюю версию 
wget https://github.com/goodwithtech/dockle/releases/download/v0.4.3/dockle_0.4.3_Linux-64bit.tar.gz && tar zxf dockle_0.4.3_Linux-64bit.tar.gz 
Выполняем сканирование ./dockle incedos_fl:latest
```output
FATAL   - CIS-DI-0010: Do not store credential in environment variables/files        * Suspicious filename found : usr/local/lib/python3.10/site-packages/pymongo/settings.py (You can suppress it with "-af settings.py")
FATAL   - DKL-DI-0005: Clear apt-get caches
        * Use 'rm -rf /var/lib/apt/lists' after 'apt-get install|update' : /bin/sh -c apt update -y && apt upgrade -y
WARN    - CIS-DI-0001: Create a user for the container
        * Last user should not be root
WARN    - DKL-DI-0006: Avoid latest tag
        * Avoid 'latest' tag
INFO    - CIS-DI-0005: Enable Content trust for Docker
        * export DOCKER_CONTENT_TRUST=1 before docker pull/build
INFO    - CIS-DI-0006: Add HEALTHCHECK instruction to the container image
        * not found HEALTHCHECK statement
INFO    - CIS-DI-0008: Confirm safety of setuid/setgid files
        * setgid file: grwxr-xr-x usr/bin/wall
        * setuid file: urwxr-xr-x usr/bin/passwd
        * setgid file: grwxr-xr-x usr/bin/expiry
        * setgid file: grwxr-xr-x usr/bin/chage
        * setuid file: urwxr-xr-x bin/su
        * setgid file: grwxr-xr-x sbin/unix_chkpwd
        * setuid file: urwxr-xr-x usr/bin/chsh
        * setuid file: urwxr-xr-x usr/bin/chfn
        * setuid file: urwxr-xr-x usr/bin/newgrp
        * setuid file: urwxr-xr-x bin/umount
        * setuid file: urwxr-xr-x bin/mount
        * setuid file: urwxr-xr-x usr/bin/gpasswd
INFO    - DKL-LI-0003: Only put necessary files
        * Suspicious directory : tmp 
        * unnecessary file : app/mongotest/docker-compose.yml 
        * unnecessary file : app/Dockerfile 
        * unnecessary file : app/docker-compose.yml
```

#####  Anchore
Сканер уязвимостей для образов контейнеров и файловых систем. Легко установите двоичный файл, чтобы опробовать его. Работает с Syft, мощным инструментом SBOM (спецификация программного обеспечения) для образов контейнеров и файловых систем.

Сканируйте содержимое образа контейнера или файловой системы, чтобы найти известные уязвимости.

docker pull anchore/grype

docker run -it --rm anchore/grype mongo

⠸ Vulnerability DB        ━━━━━━━━━━━━━━━━━━━━━━━  [86 MB / 90 MB]
 ⠴ Parsing image           ━━━━━━━━━━━━━━━━━━━━━━━

```output
 ✔ Vulnerability DB        [updated]
 ✔ Parsed image             ✔ Cataloged packages      [153 packages]
 ✔ Scanned image           [52 vulnerabilities]
NAME                  INSTALLED                 FIXED-IN  VULNERABILITY     SEVERITY   
bash                  5.0-6ubuntu1.1                      CVE-2019-18276    Low         
coreutils             8.30-3ubuntu2                       CVE-2016-2781     Low         
krb5-locales          1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
krb5-locales          1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libasn1-8-heimdal     7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2020-6096     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-3326     Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2016-10228    Negligible  
libc-bin              2.31-0ubuntu9.2                     CVE-2021-35942    Medium      
libc-bin              2.31-0ubuntu9.2                     CVE-2021-33574    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-38604    Medium      
libc-bin              2.31-0ubuntu9.2                     CVE-2020-27618    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2021-27645    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2020-29562    Low         
libc-bin              2.31-0ubuntu9.2                     CVE-2019-25013    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2020-6096     Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-3326     Low         
libc6                 2.31-0ubuntu9.2                     CVE-2016-10228    Negligible  
libc6                 2.31-0ubuntu9.2                     CVE-2021-35942    Medium      
libc6                 2.31-0ubuntu9.2                     CVE-2021-33574    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-38604    Medium      
libc6                 2.31-0ubuntu9.2                     CVE-2020-27618    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2021-27645    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2020-29562    Low         
libc6                 2.31-0ubuntu9.2                     CVE-2019-25013    Low         
libgmp10              2:6.2.0+dfsg-4                      CVE-2021-43618    Low         
libgssapi-krb5-2      1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libgssapi-krb5-2      1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libgssapi3-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libhcrypto4-heimdal   7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libheimbase1-heimdal  7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libheimntlm0-heimdal  7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libhx509-5-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libk5crypto3          1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libk5crypto3          1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libkrb5-26-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libkrb5-3             1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libkrb5-3             1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libkrb5support0       1.17-6ubuntu4.1                     CVE-2021-36222    Medium      
libkrb5support0       1.17-6ubuntu4.1                     CVE-2018-5709     Negligible  
libpcre3              2:8.39-12build1                     CVE-2020-14155    Negligible  
libpcre3              2:8.39-12build1                     CVE-2017-11164    Negligible  
libroken18-heimdal    7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9991     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9849     Low         
libsqlite3-0          3.31.1-4ubuntu0.2                   CVE-2020-9794     Medium      
libtasn1-6            4.16.0-2                            CVE-2018-1000654  Negligible  
libwind0-heimdal      7.7.0+dfsg-1ubuntu1                 CVE-2021-3671     Low         
login                 1:4.8.1-1ubuntu5.20.04.1            CVE-2013-4235     Low         
passwd                1:4.8.1-1ubuntu5.20.04.1            CVE-2013-4235     Low         
perl-base             5.30.0-9ubuntu0.2                   CVE-2020-16156    Medium      
```

##### Docker Bench for Security
The Docker Bench for Security is a script that checks for dozens of common best-practices around deploying Docker containers in production. The tests are all automated, and are based on the CIS Docker Benchmark v1.3.1.

Для запуска утилиты воспользуйтесь следующим образом
```
docker run --rm --net host --pid host --userns host --cap-add audit_control \
    -e DOCKER_CONTENT_TRUST=$DOCKER_CONTENT_TRUST \
    -v /etc:/etc:ro \
    -v /lib/systemd/system:/lib/systemd/system:ro \
    -v /usr/bin/containerd:/usr/bin/containerd:ro \
    -v /usr/bin/runc:/usr/bin/runc:ro \
    -v /usr/lib/systemd:/usr/lib/systemd:ro \
    -v /var/lib:/var/lib:ro \
    -v /var/run/docker.sock:/var/run/docker.sock:ro \
    --label docker_bench_security \
    docker/docker-bench-security
```
Утилита проверяет приложения по следующим параметрам:
- Host Configuration
- Docker daemon configuration
- Docker daemon configuration files
- Container Images and Build File
- Container Runtime
- Docker Security Operations
- Docker Swarm Configuration

```
# ------------------------------------------------------------------------------
# Docker Bench for Security v1.3.4
#
# Docker, Inc. (c) 2015-
#
# Checks for dozens of common best-practices around deploying Docker containers in production.
# Inspired by the CIS Docker Community Edition Benchmark v1.1.0.
# ------------------------------------------------------------------------------

Initializing Thu Dec  9 07:07:03 UTC 2021


[INFO] 1 - Host Configuration
[WARN] 1.1  - Ensure a separate partition for containers has been created
[NOTE] 1.2  - Ensure the container host has been Hardened
[INFO] 1.3  - Ensure Docker is up to date
```


##### Clair
Clair - это приложение для анализа содержимого изображений и сообщения об уязвимостях, влияющих на содержимое. Это делается с помощью статического анализа, а не во время выполнения.



*Практические задания для текущего занятия ч.1*
1. Воспольщуйтесь линтером HADOLINT для анализа Dockerfile из предыдущих работ.
2. Воспользуйтесь приложением Dive для анлиза слоев вашего образа.
3. Представьте отчет о безопасности вашего приложения из Лабораторной 6.
4. Воспользуйтесь образом anchore/grype для анализа уязвимостей из приложения, используемого в задании 3.

Дополнительные источники
1. https://github.com/hadolint/hadolint
2. https://docs.docker.com/develop/scan-images/
3. https://swordfishsecurity.ru/blog/
4. obzor-utilit-bezopasnosti-docker
5. https://techrocks.ru/2020/04/27/docker-image-security/

*Практические задания для главы docker ч.2*
1. Описать Dockerfile, который содержит Python приложение (или, например, можно использовать контейнер со статической веб-страницей 

2. Исправьте неправильно написанный Dockerfile. Есть условное Node.js приложение, и неправильно написанный Dockerfile, который не будет кэшироваться и будет занимать много места. Нужно переписать его в соответствии с best-practice
```
#плохой файл 
FROM ubuntu:18.04 
COPY ./src /app 
RUN apt-get update -y 
RUN apt-get install -y nodejs 
RUN npm install 
ENTRYPOINT ["npm"] 
CMD ["run", "prod"]
```

Практические задания 

command: 'sh -c echo 'hello''

