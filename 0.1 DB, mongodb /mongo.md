##### MONGO


docker run -d -p 27017:27017 --name some-mongo \
    -e MONGO_INITDB_ROOT_USERNAME=mongoadmin \
    -e MONGO_INITDB_ROOT_PASSWORD=secret \
    mongo


Подключение 
1. Расширение mongodb в vscode
2. 
Заходим в контейнер
xub1@xub1:~/mongo$ docker exec -it some-mongo bash
Подключаемся
root@468eb81e6297:/#  


docker exec -it name-container bash

Мы попали внутрь контейнера, подключимся к базе от созданного пользователя 
mongo --host localhost:27017 -p secret -u mongoadmin

MongoDB shell version v5.0.3
connecting to: mongodb://localhost:27017/?compressors=disabled&gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("74495428-73ba-49ec-a890-ea24a1a2768a") }
MongoDB server version: 5.0.3

Warning: the "mongo" shell has been superseded by "mongosh",
which delivers improved usability and compatibility.The "mongo" shell has been deprecated and will be removed in
an upcoming release.
We recommend you begin using "mongosh".
For installation instructions, see
https://docs.mongodb.com/mongodb-shell/install/


        The server generated these startup warnings when booting: 
        2021-11-14T06:19:23.587+00:00: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine. See http://dochub.mongodb.org/core/prodnotes-filesystem


        Enable MongoDB's free cloud-based monitoring service, which will then receive and display
        metrics about your deployment (disk utilization, CPU, operation statistics, etc).

        The monitoring data will be available on a MongoDB website with a unique URL accessible to you
        and anyone you share the URL with. MongoDB may use this information to make product
        improvements and to suggest MongoDB products and deployment options to you.

        To enable free monitoring, run the following command: db.enableFreeMonitoring()
        To permanently disable this reminder, run the following command: db.disableFreeMonitoring()


Выведем список всех db 
```mongodb
> show dbs
NEW_DATABASE_NAME  0.000GB
admin              0.000GB
config             0.000GB
local              0.000GB
```

команда  db, отобразит текущую базу данных

```mongodb
> db
test
```

Если база данных не существует, MongoDB создает базу данных при первом сохранении данных для этой базы данных. Таким образом, вы можете переключиться на несуществующую базу данных и выполнить в mongosh следующую операцию:

```mongodb
> use myDB
switched to db myDB
> db
myDB
```



##### 

Коннект к mongo из python

from pymongo import MongoClient
# pprint library is used to make the output look more pretty
from pprint import pprint
# connect to MongoDB, change the << MONGODB URL >> to reflect your own connection string
# входим внутрь контейнера коннектимся из под пользователя, копируем ссылку и добавляем в коннект devroot:devroot
client = MongoClient('mongodb://devroot:devroot@localhost:27017/?compressors=disabled&gssapiServiceName=mongodb')
db=client.admin
# Issue the serverStatus command and print the results
serverStatusResult=db.command("serverStatus")
pprint(serverStatusResult)   