### install k8s bare metal, Kubespray

Установку будем производить на 2 виртуальные машины,

Перейдем на каждую из машин и установим 
sudo apt install openssh-server

Проверим, что служба ssh работает
systemctl status sshd

Создадим ключи и скопируем публичный на каждую из машин

Создадим, ключ. В первом вопросе укажите путь до ключа например `/home/ub/.ssh/id_rsa_251`

ssh-keygen

Скопируем ключ на каждую из машин

ssh-copy-id -i /home/ub/.ssh/id_rsa_251.pub minikube@192.168.122.251


Предыдущие шаги позволили подконнетить по ssh две машины. 
Выберите в качестве masternode одну машину в качестве workernode другую

выполним обновление машин

sudo apt update -y && sudo apt upgrade -y

Установим docker или другую среду для выполнения контейнеров

Установим kubectl

https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/#install-kubectl-binary-with-curl-on-linux



https://habr.com/ru/post/462473/


### Установка на Ubuntu 20.04

https://www.cloudsigma.com/how-to-install-and-use-kubernetes-on-ubuntu-20-04/

### Установка Calico
sudo kubeadm init устанавливать в Calico

https://projectcalico.docs.tigera.io/getting-started/kubernetes/quickstart

troubleshuting command `kubectl get pods --all-namespaces`


### Удаление
kubectl drain kubernetes-master --delete-local-data --force --ignore-daemonsets

kubectl delete node kubernetes-master

sudo kubeadm reset -f

sudo rm -rf /etc/cni /etc/kubernetes /var/lib/dockershim /var/lib/etcd /var/lib/kubelet /var/run/kubernetes ~/.kube/*

sudo iptables -F && sudo iptables -X && sudo iptables -t nat -F && sudo iptables -t nat -X && sudo iptables -t raw -F && sudo iptables -t raw -X && sudo iptables -t mangle -F && sudo iptables -t mangle -X

sudo ipvsadm --clear

sudo apt-get purge kubeadm kubectl kubelet kubernetes-cni kube*   
sudo apt-get autoremove  

https://questu.ru/questions/44698283/


sudo systemctl enable docker
sudo systemctl daemon-reload
sudo systemctl restart docker
