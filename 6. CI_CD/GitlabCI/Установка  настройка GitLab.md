Gitlab CI/CD


1.1. Погружение в CI/CD
2.1. Принципы работы CI и CD
2.2. Быстрый старт с Gitlab
3.1. Инструменты для построения CI/CD 1
3.1. Инструменты для построения CI/CD 2
4.1. Из чего состоит Gitlab и как его установить
5.1. Рекомендации по работе с Gitlab
5.2. Первый проект в Gitlab на практике
6.1. Задачи и возможности gitlab runner, его установка
7.1. Основные понятия при написании файла конфигурации
7.2. Пишем простой файл .gitlab-ci.yml: сборка, тесты, развертывание 1
7.2. Пишем простой файл .gitlab-ci.yml: сборка, тесты, развертывание 2
7.3. Deploy приложения в кластер Kubernetes
8.1. Продвинутые приемы работы с Gitlab CI/CD pipeline
9.1. Добавление в пайплайн возможности Rollback
9.2. Работа с динамическими окружениями
10.1. Интеграция Gitlab CI/CD и кластера Kubernetes
11.1. Push и Pull модели работы; применяем ArgoCD на практике
12.1. Скрытие переменных CI/CD, интеграция с Vault 1
12.1. Скрытие переменных CI/CD, интеграция с Vault 2
12.2. Статический анализ кода на безопасность (SAST) 1
12.2. Статический анализ кода на безопасность (SAST) 2
AMA-сессия к видеокурсу по CI/CD



#### Первоначальная нстройка хоста

установить git

если ключ не добавлен добавим
add ssh key 

generate key localhost
add key gitlab

перед первым комит выполним

  git config --global user.email "you@example.com"
  git config --global user.name "Your Name"

,где вместо you@example.com и Your Name укажите свои данные


#### Создадим тестовый проект
#### Установка runner

*Docker*
```bash
docker run -d --name gitlab-runner --restart always  -v /srv/gitlab-runner/config:/etc/gitlab-runner   -v /var/run/docker.sock:/var/run/docker.sock   gitlab/gitlab-runner:latest
```
*Docker-compose*
```
version: '3'
services:
  gitlab-runner:
    image: 'gitlab/gitlab-runner:latest'
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./config:/etc/gitlab-runner
    restart: unless-stopped
networks:
  default:
    external:
      name: www-network
```

#### Регистрация runner
Откроем настройки Gitlab CI/CD

Settings-> Runners
Представленый вывод сообщит информацию для регистрации gitlab runners

```
Set up a specific Runner for a project

    1) Install GitLab Runner and ensure it's running.
    2) Register the runner with this URL:
    http://mobilsrv.iict.ru:888/ 
    
    And this registration token:
    p8FJP8gwwfDegasvHjdA
```


проваливаемся внутрь контейнера 
docker exec -it name_container_gitlab runner sh
Введемтокен и ссылку из gitlab

bash-5.0# gitlab-runner register
Runtime platform                                    arch=amd64 os=linux pid=61 revision=98daeee0 version=14.7.0
Running in system-mode.                            
                                                   
Enter the GitLab instance URL (for example, https://gitlab.com/):
http://mobilsrv.iict.ru:888/
Enter the registration token:
p8FJP8gwwfDegasvHjdA
Enter a description for the runner:
[7ed4f1de1e52]: cicd-test
Enter tags for the runner (comma-separated):
dev-shell        
Registering runner... succeeded                     runner=p8FJP8gw
Enter an executor: shell, ssh, virtualbox, docker-ssh+machine, custom, docker, docker-ssh, parallels, docker+machine, kubernetes:
docker
Enter the default Docker image (for example, ruby:2.6):
alpine:latest
Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded! 


Убедимся что все установлено
bash-5.0# gitlab-runner list
```
Runtime platform                                    arch=amd64 os=linux pid=86 revision=98daeee0 version=14.7.0
Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
cicd-test                                           Executor=docker Token=ycAsCGs9YsktVdzRYWkr URL=http://mobilsrv.iict.ru:888/
```

#### Настройка runner

После регистрации запущеный runner будет отображаться по тому же пути
Settings-> Runners

При установке runnera был указан тег, который говорит gitlab запускать CI/CD pinepline только с данным тегом.

После установки рядом с runner будет вывешен статус замок, для разблокирования и запуска pinipline с любым тегом. Установите галочку в поле Indicates whether this runner can pick jobs without tags. И снемите голочку с поля When a runner is locked, it cannot be assigned to other projects.

Для использования runnera в других проектах, пройдите к настройкам runner нового проекта и нажмите Enable.


Конфигурация runner

config/config.toml
```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "docker-stable"
  url = "http://mobilsrv.iict.ru:888/"
  token = "FhEARm39vAKYDC6_TNh_"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache", "/var/run/docker.sock:/var/run/docker.sock"]
    shm_size = 0
```

#### Установка и настройка docker regystry
```
version: "2"
services:
  registry:
    image: registry:2
    environment:
      - REGISTRY_HTTP_SECRET=o43g2kjgn2iuhv2k4jn2f23f290qfghsdg
      - REGISTRY_STORAGE_DELETE_ENABLED=true
    volumes:
      - ./registry-data:/var/lib/registry
    ports:
      - '9000:5000'
    restart: always
  ui:
    image: jc21/registry-ui
    environment:
      - NODE_ENV=production
      - REGISTRY_HOST=registry:5000
    #   - REGISTRY_SSL=true
    #   - REGISTRY_DOMAIN=registry
    #   - REGISTRY_STORAGE_DELETE_ENABLED=true
    links:
      - registry
    restart: always
  proxy:
    image: jc21/registry-ui-proxy
    ports:
      - 9002:80
    depends_on:
      - ui
      - registry
    links:
      - ui
      - registry
    restart: always
networks:
  default:
    external:
      name: www-network
```

Протестируйте подключение из под локальной машны с слудующими настройками 
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
sudo echo { "insecure-registries":["172.17.0.1:5005"] } > /etc/docker/daemon.json  


#### Настройка кэша

При повторном pinepline, все операции повторяются. Для сборок docker образов это является критичным в связи с этим необходимо хранить состояние pinepline.

Minio

https://www.tremplin-numerique.org/ru/comment-configurer-minio-en-tant-que-cache-partage-pour-gitlab-ci


