#### Ansible 

[Установка Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#installing-ansible-on-ubuntu)


```
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt install ansible
```

##### Конфигурирование проекта

Cоздадим каталог с проектом

Cоздадим в каталоге файл с конфгуррацией `ansible.cfg`, данный файл необходим для того чтобы Ansible не смотрел в глобальный конфиг, а использовал локальный.

```
[defaults]

inventory = $(pwd)/hosts
host_key_checking = false
````

Cоздадим hostfile с названием `hosts`

Предварительно создадим ssh ключ и забросьте его в файл authorization_key в каталог ./ssh на каждую из машин.

```
client1prod ansible_host=192.168.0.13 ansible_user=vagrant ansible_ssh_private_key_file=/home/xub/.ssh/id_rsa_tv
client2test ansible_host=192.168.0.14 ansible_user=vagrant ansible_ssh_private_key_file=/home/xub/.ssh/id_rsa_tv
```

Пропингуем машины
`ansible -i hosts all -m ping`


##### Разобьем хосты по группам

```
[prod]
client1prod ansible_host=192.168.0.13 ansible_user=vagrant ansible_ssh_private_key_file=/home/xub/.ssh/id_rsa_tv
[test]
client2test ansible_host=192.168.0.14 ansible_user=vagrant ansible_ssh_private_key_file=/home/xub/.ssh/id_rsa_tv
```

##### Работа с модулями

Просмотрим информацию о настройках хостов
`ansible -i hosts all -m setup`

Выполним команду bash сразу для всех хостов
`ansible -i hosts all -m shell -a "ls -la"`

`ansible -i hosts all -m command -a "ls -la"`

Создадим файл на всех хостах

`ansible -i hosts all -m file -a "path=/home/ansible-file.txt state=touch"`

`ansible -i hosts all -m file -a "path=/home/ansible-file.txt state=touch" -b`

Если возникают проблемы с выполнением команд, добавьте текущего пользователя в группу sudo

Создайте в текущей директории файл file_copy. Скопируйте его в директорию home с правами 777

`ansible -i hosts all -m copy -a "src=file_copy dest=/home mode=777" -b`


##### Переменные 

создадим каталог group_vars
в ней создадим файлы для группы test, prod

вынесем в файлы переменные из файла hosts
```
ansible_user: vagrant 
ansible_ssh_private_key_file: /home/xub/.ssh/id_rsa_tv
```

Отредактируем hosts файл
```
[prod]
client1prod ansible_host=192.168.0.13
[test]
client2test ansible_host=192.168.0.14
```
Протестируем
ansible all -m ping 

Просмотрим переменные
ansible all -m debug -a "var=ansible_user"

ansible all -m debug -a "var=ansible_host"



##### Первый playbook

touch ping.yml

```ansible
- name: Ping Servers
  hosts: all
  become: yes

  tasks:
  - name: Task ping
    ping:
```

пропингуем и обновим


```
- name: Ping Servers
  hosts: all
  become: yes

  tasks:
  - name: Task ping
    ping:

  - name: Update cache
    apt:
      update_cache: yes
      
  - name: Upgrade
    apt:
      upgrade: yes
    become: yes
```

установка
```
- name: Ping Servers
  hosts: all
  become: yes

  tasks:
  - name: Task ping
    ping:

  - name: Upgrade
    apt:
      upgrade: yes
    become: yes

  - name: Install apache
    apt:
      pkg: apache2
      state: present
    become: yes

копирование файла

 - name: copy
    copy: 
      src: ./file_copy1
      dest: ./home
      mode: 0777
```

##### Облегчим адниминистрирование, установим [UI для Ansible](https://docs.ansible-semaphore.com/administration-guide/installation)


*Практические задания*
1. Используя Vagrant разверните 2 виртуальные машины.
https://www.youtube.com/watch?v=YYjCwLs-1hA

