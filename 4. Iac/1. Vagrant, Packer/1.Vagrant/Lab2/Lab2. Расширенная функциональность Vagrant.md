#### Расширенная функциональность Vagrant

Предыдущая практика позволила познакомиться с основными функция Vagrant.
Данное практиское занятие позволит углубиться в данный инструмент.

##### Дополнительные команды CLI Vagrant

Из первой практики очевидно, что формат использования образов в Vagrant .box.
Научимся ими управлять.

Просмотр сприска образов
```bash
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.5.4)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
```

Как и в docker, так и у боксов Vagrant существуют свои версии.
Проверим доступность новой версии в боксе для провайдера libvirt. Стоит заметить, что данная команда отработает в случа, если в текущей директории существет Vagrantfile. 

```bash
xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$ vagrant box outdated
Checking if box 'generic/centos8' version '3.5.4' is up to date...
A newer version of the box 'generic/centos8' for provider 'libvirt' is
available! You currently have version '3.5.4'. The latest is version
'3.6.6'. Run `vagrant box update` to update.
```

В выводе команды, просят обновить бокс командой vagrant box update.

Данная команда, не обновляет старый образ, а скачивает новый.
```bash
xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$  vagrant box list
generic/centos8    (libvirt, 3.5.4)
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
```
Удалить старые образы возможно командой 

xub1@xub1:~/project/devops/4. Iac/1.Vagrant, Packer/1.Vagrant/Lab1$ vagrant box prune
```bash
The following boxes will be kept...
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)

Checking for older boxes...
Box 'generic/centos8' (v3.5.4) with provider 'libvirt' appears
to still be in use by at least one Vagrant environment. Removing
the box could corrupt the environment. We recommend destroying
these environments first:

test (ID: 4cc3061358ad4ae2815e04302dc2aab0)

Are you sure you want to remove this box? [y/N] y
Removing box 'generic/centos8' (v3.5.4) with provider 'libvirt'...
```

Все боксы в Linux и MacOS храняться по следующему пути
```bash
xub1@xub1:~/.vagrant.d/boxes$ ls
generic-VAGRANTSLASH-centos8  generic-VAGRANTSLASH-ubuntu1604  ubuntu-VAGRANTSLASH-trusty64
```
В Windows C:/Users/USERNAME/.vagrant.d/boxes./

Просмотреть, сколько занимает памяти бокс можно перейдя по пути и выполнив команду `ls -lh`.
```bash
xub1@xub1:~/.vagrant.d/boxes/ubuntu-VAGRANTSLASH-trusty64/20190514.0.0/virtualbox$ ls -lh
итого 429M
-rw------- 1 xub1 xub1 429M дек 19 07:11 box-disk1.vmdk
-rw------- 1 xub1 xub1  11K дек 19 07:11 box.ovf
-rw-rw-r-- 1 xub1 xub1   25 дек 19 07:11 metadata.json
-rw-r--r-- 1 xub1 xub1  505 дек 19 07:11 Vagrantfile
```
Либо проверить размер каталога с его содержимым командай `du -sh`
```bash
xub1@xub1:~/.vagrant.d/boxes$ du -sh ubuntu-VAGRANTSLASH-trusty64/
429M    ubuntu-VAGRANTSLASH-trusty64/
```

Для удаления бокса используйте команду remove
```vagrant box remove ubuntu/trusty64```
```bash
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
ubuntu/trusty64    (virtualbox, 20190514.0.0)
xub1@xub1:~/project/devops$ vagrant box remove ubuntu/trusty64
Removing box 'ubuntu/trusty64' (v20190514.0.0) with provider 'virtualbox'...
xub1@xub1:~/project/devops$ vagrant box list
generic/centos8    (libvirt, 3.6.6)
generic/ubuntu1604 (libvirt, 3.6.4)
```
На предыдущей практике мы обеспечивали надлежащие состояние машины при помоще скрипта, что если все что-то пошло не так и не выполнились инструкции. Для этого существует команда `vagrant provision`, которая при работающей ВМ, позволяет пересобрать конфигурацию машины с учетом исправлений. Стоит учесть, что первоначально выполненные команды при развертывании определяют текущее состояние машины.

Как и docker Vagrant в основном предназначен для быстрого создания тестовых стендов. Соответственно для разработчиков необходимо предоставить полностью настроенную среду. Мы уже умеем устанавливать и настраивать необходимое окружение, осталось научиться монтировать каталоги, для того чтобы разработчик умел локально разрабатывать и сразу просматривать готовый результат на стенде. Для этого существует возможность пробразывать каталог внутрь ВМ.

`test.vm.synced_folder "./html", "/var/www/html"`
Если извне постучаться на порт 80 nginx, то мы увидим полностью новую страничку html. Более подробно о настроке веб сервера Nginx пройдем на последующих практиках.

##### Использование переменные и создание нескольких машин

В теоретических сведениях предыдущей практики вы видели пример с развертыванием нескольких машин в одном Vagrantfile.
Рассмотрим следующий пример, предположим мы хотим развернуть несколько инстенсов базы данных в случае утери одного данные бы надежно были сохранены.
```Vagrantfile
Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = 1024  
    lv.cpus = 1
  end
  (1..2).each do |i|   #
  config.vm.define "prod#{i}" do |prod| #
  prod.vm.box = "generic/ubuntu1610"
  prod.vm.hostname = "node#{i}"
  end
 end
end
```
В данном примере используем следующую конструкцию, представляющую собой цикл `(1..2).each do |i|`, который позволяет развертывать виртуальные машины друг за другом. Значения 1 и 2 будут попадать в переменную i, каждое в своей итерации.
Поэтому насвание виртуальных машин будет prod1 и prod2, а имя хоста будет node1 и node2.

Вышеприведенный пример, использует переменную i в качестве идентификатора каждой из машин, для удобства настройки Vagrantfile используются переменые определяемые в самом начале манифеста. Данный подход, очень удопен при настройке множества виртуальных машин.

```Vagrantfile
BOX_IMAGE = "generic/ubuntu1610"
NODE_COUNT = 2

Vagrant.configure("2") do |config|
  config.vm.provider "libvirt" do |lv|
    lv.memory = 1024  
    lv.cpus = 1
  end
  (1..NODE_COUNT).each do |i|
  config.vm.define "prod#{i}" do |prod|
  prod.vm.box = BOX_IMAGE
  prod.vm.hostname = "#{NODE_COUNT}#{i}"
  end
 end
end
```

*Практические задания*
1. Проверьте доступны ли новые боксы.
2. Удалите все боксы занимающие больше 700Mb метса на диске.
3. Создайте общий каталог для гостевой машины и хостовой, пробростьте произвольный index.html и убедитесь, что при его изменении, соджержимое изменяется на сервере nginx.
4. Разверните две машины, используя переменные установите на одну СУБД PostgeSql, на другую apache.
5. Развертите две одинаковые машины, установив на которые СУБД MongoDB, используя переменные. 
Практические задания считаются выполеными, если вы успешно проверили работоспособность установленного програамного обеспечения, для этого рекомендуется и этот процесс автоматизировать добавив необходимые конструкции в инструкцию в скрипт.

Литература
https://www.vagrantup.com/docs/cli/box#box-prune
https://help.ubuntu.ru/wiki/vagrant
http://alexconst.net/2016/01/28/vagrant/
https://github.com/vagrant-libvirt/vagrant-libvirt