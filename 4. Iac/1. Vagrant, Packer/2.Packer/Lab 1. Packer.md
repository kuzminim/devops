#### Packer
Packer позволяет создавать образы дисков виртуальных машин с заданными в конфигурационном файле параметрами. Сценарий описывает создание образа диска с помощью Packer.

##### Установка

Установка из репозитория
`sudo apt install packer`

Проверим версию
`packer --version`

Посмотрим список команд следующей командой в терминале
```bash
xub1@xub1:~$ packer
Usage: packer [--version] [--help] <command> [<args>]

Available commands are:
    build           build image(s) from template
    console         creates a console for testing variable interpolation
    fix             fixes templates from old versions of packer
    fmt             Rewrites HCL2 config files to canonical format
    hcl2_upgrade    transform a JSON template into an HCL2 configuration
    init            Install missing plugins or upgrade plugins
    inspect         see components of a template
    validate        check that a template is valid
    version         Prints the Packer version
```

https://github.com/jedi4ever/veewee

https://learn.hashicorp.com/tutorials/packer/docker-get-started-build-image?in=packer/docker-get-started

https://infostart.ru/1c/articles/1201632/

packer init .

packer fmt .

packer validate

packer build docker-ubuntu.pkr.hcl

```
docker images
REPOSITORY                     TAG                IMAGE ID       CREATED              SIZE
<none>                         <none>             bc233e41ea65   About a minute ago   135MB
```

sudo apt-get install -y virtualbox-guest-additions-iso

https://www.packer.io/plugins/builders/qemu

https://www.youtube.com/watch?v=TmMoTseT2Ow&list=PL8VzFQ8k4U1Jp6eWgHSXHiiRWRvPyCKRj&index=6


https://github.com/heizo/packer-ubuntu-20.04

preseed.cfg path

https://codeblog.dotsandbrackets.com/build-image-packer/